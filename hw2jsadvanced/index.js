class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get salary() {
        return this._salary
    }

    set salary(value) {
        this._salary = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

let programmers = [
    new Programmer('Tonny', 22, 3000, ['eng', 'rus', 'ukr']),
    new Programmer('Den', 29, 10000, ['eng', 'rus', 'ukr', 'spanish']),
    new Programmer('Egor', 19, 200, ['ukr'])
];
programmers.forEach(elem => {
    console.log(elem);
    console.log(`employee salary getter ${elem.salary}`);
});