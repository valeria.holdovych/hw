'use strict'

const error = document.querySelector('.error');

function cleanError() {
    error.innerHTML = '';
}

function addError() {
    error.append('Wrong Password');
    error.classList.add("error");
}

function logSubmit(event) {
    const password = document.getElementById('get-password').value;
    const repeatPassword = document.getElementById('repeat-password').value;
    cleanError();
    if (password !== repeatPassword) {
        addError();
    } else {
        alert('you are welcome');
    }
    event.preventDefault();
}

function changeInput(eye) {
    const label = eye.closest("label");
    const input = label.querySelector('input');
    if (eye.classList.contains('fa-eye-slash')) {
        input.setAttribute('type', 'text');
        eye.className = 'fas fa-eye icon-password js-icon';
    } else {
        input.setAttribute('type', 'password');
        eye.className = 'fas fa-eye-slash icon-password js-icon';
    }
}

const checkBtn = document.getElementById('form');
checkBtn.addEventListener('submit', logSubmit)

const form = document.querySelectorAll('form input');
form.forEach(e => {
    e.addEventListener('click', cleanError)
})

const eyes = document.querySelectorAll('.js-icon');
eyes.forEach(function (e) {
    e.addEventListener('click', function () {
        changeInput(e)
    })
})







