'use strict'

const textContainer = document.querySelectorAll('.tabs-content li')
const liCollection = document.querySelectorAll('.tabs li');

liCollection.forEach(li => {
    li.addEventListener('click', (event) => {
        document.querySelector('.active').classList.remove('active');
        event.target.classList.add('active');

        textContainer.forEach(ul =>{
            ul.classList.remove('active-text');
        })

        const currentLiAtt = li.getAttribute('data-category');
        const currentElement = document.getElementById(currentLiAtt);
        currentElement.classList.add('active-text');
    })
});





