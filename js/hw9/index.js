'use strict'


const createArr = (arr, parent) => {
    const map = arr.map(elem =>{
        const parentLi = document.createElement(parent);
        parentLi.innerText = elem;
        document.body.appendChild(parentLi);
    })
}

const testArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const testParent = 'li';
createArr(testArr, testParent);
