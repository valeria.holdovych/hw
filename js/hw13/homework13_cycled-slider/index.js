'use strict'

const stopShow = document.querySelector('#stop-show');
const startShow = document.querySelector('#start-show');
const image = document.querySelector('#image');
const hideImages = ["img/1.jpg", "img/2.jpg", "img/3.JPG", "img/4.png"];

let counter = 0;
let timer = interval();

function interval() {
    return setInterval(function () {
        slide();
    }, 3000);
}

function slide() {
    if (counter === hideImages.length - 1) {
        counter = 0;
    } else {
        counter++;
    }
    image.src = hideImages[counter];
}

stopShow.addEventListener('click', function () {
    clearInterval(timer);
});

startShow.addEventListener('click', function () {
    timer = interval();
});