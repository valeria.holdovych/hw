'use strict'

const theme = document.querySelector('#theme');
const changeModeBtn = document.querySelector('.btn-change-mode');
setCurrentTheme();

function setCurrentTheme() {
    let mode = localStorage.getItem('mode') === null ? 'light' : localStorage.getItem('mode');
    if (mode === 'light') {
        theme.href = 'css/style.css';
    } else {
        theme.href = 'css/styleBlack.css';
    }
    localStorage.setItem('mode', mode);
}

function changeTheme() {
    let mode = localStorage.getItem('mode');
    if (mode === 'light') {
        theme.href = 'css/styleBlack.css';
        mode = 'dark';
    } else {
        theme.href = 'css/style.css';
        mode = 'light';
    }
    localStorage.setItem('mode', mode);
}

changeModeBtn.addEventListener('click', function () {
    changeTheme();
});

