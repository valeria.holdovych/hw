'use strict'

const products = [
    // graphic design
    {
        id: 1,
        imgSrc: './images/landing-page2.jpg',
        category: 'Graphic Design',
    },
    {
        id: 2,
        imgSrc: './images/Layer 24.png',
        category: 'Graphic Design',
    },
    {
        id: 3,
        imgSrc: './images/Layer 25.png',
        category: 'Graphic Design',
    },
    {
        id: 4,
        imgSrc: './images/Layer 26.png',
        category: 'Graphic Design',
    },
    {
        id: 5,
        imgSrc: './images/Layer 27.png',
        category: 'Graphic Design',
    },
    {
        id: 6,
        imgSrc: './images/Layer 28.png',
        category: 'Graphic Design',
    },
    {
        id: 7,
        name: 'Product 6',
        imgSrc: './images/Layer 29.png',
        category: 'Graphic Design',
    },
    {
        id: 8,
        imgSrc: './images/Layer 30.png',
        category: 'Graphic Design',
    },
    {
        id: 9,
        imgSrc: './images/Layer 31.png',
        category: 'Graphic Design',
    },
    {
        id: 10,
        imgSrc: './images/Layer 32.png',
        category: 'Graphic Design',
    },
    {
        id: 11,
        imgSrc: './images/Layer 33.png',
        category: 'Graphic Design',
    },
    {
        id: 12,
        imgSrc: './images/Layer 24.png',
        category: 'Graphic Design',
    },

    //web design

    {
        id: 13,
        imgSrc: './web design/web-design3.jpg',
        category: 'Web Design',
    },
    {
        id: 14,
        imgSrc: './web design/web-design5.jpg',
        category: 'Web Design',
    },
    {
        id: 15,
        imgSrc: './web design/web-design1.jpg',
        category: 'Web Design',
    },
    {
        id: 16,
        imgSrc: './web design/web-design7.jpg',
        category: 'Web Design',
    },
    {
        id: 17,
        imgSrc: './web design/web-design4.jpg',
        category: 'Web Design',
    },
    {
        id: 18,
        imgSrc: './web design/web-design4.jpg',
        category: 'Web Design',
    },
    {
        id: 19,
        imgSrc: './web design/web-design1.jpg',
        category: 'Web Design',
    },
    {
        id: 20,
        imgSrc: './web design/web-design7.jpg',
        category: 'Web Design',
    },
    {
        id: 21,
        imgSrc: './web design/web-design5.jpg',
        category: 'Web Design',
    },
    {
        id: 22,
        imgSrc: './web design/web-design4.jpg',
        category: 'Web Design',
    },
    {
        id: 23,
        imgSrc: './web design/web-design3.jpg',
        category: 'Web Design',
    },
    {
        id: 24,
        imgSrc: './web design/web-design1.jpg',
        category: 'Web Design',
    },

    // landing pages

    {
        id: 25,
        imgSrc: './landing page/landing-page7.jpg',
        category: 'landing Pages',
    },
    {
        id: 26,
        imgSrc: './landing page/landing-page6.jpg',
        category: 'landing Pages',
    },
    {
        id: 27,
        imgSrc: './landing page/landing-page3.jpg',
        category: 'landing Pages',
    },
    {
        id: 28,
        imgSrc: './landing page/landing-page4.jpg',
        category: 'landing Pages',
    },
    {
        id: 29,
        imgSrc: './landing page/landing-page7.jpg',
        category: 'landing Pages',
    },
    {
        id: 30,
        imgSrc: './landing page/landing-page6.jpg',
        category: 'landing Pages',
    },
    {
        id: 31,
        imgSrc: './landing page/landing-page3.jpg',
        category: 'landing Pages',
    },
    {
        id: 32,
        imgSrc: './landing page/landing-page7.jpg',
        category: 'landing Pages',
    },
    {
        id: 33,
        imgSrc: './landing page/landing-page6.jpg',
        category: 'landing Pages',
    },
    {
        id: 34,
        imgSrc: './landing page/landing-page4.jpg',
        category: 'landing Pages',
    },
    {
        id: 35,
        imgSrc: './landing page/landing-page3.jpg',
        category: 'landing Pages',
    },
    {
        id: 36,
        imgSrc: './images/landing-page2.jpg/',
        category: 'landing Pages',
    },

    // wordpress
    {
        id: 37,
        imgSrc: './images/landing-page2.jpg',
        category: 'wordpress',
    },
    {
        id: 38,
        imgSrc: './wordpress/wordpress1.jpg',
        category: 'wordpress',
    },
    {
        id: 39,
        imgSrc: './wordpress/wordpress10.jpg',
        category: 'wordpress',
    },
    {
        id: 40,
        imgSrc: './wordpress/wordpress9.jpg',
        category: 'wordpress',
    },
    {
        id: 41,
        imgSrc: './wordpress/wordpress8.jpg',
        category: 'wordpress',
    },
    {
        id: 42,
        imgSrc: './wordpress/wordpress7.jpg',
        category: 'wordpress',
    },
    {
        id: 43,
        imgSrc: './wordpress/wordpress6.jpg',
        category: 'wordpress',
    },
    {
        id: 44,
        imgSrc: './wordpress/wordpress5.jpg',
        category: 'wordpress',
    },
    {
        id: 45,
        imgSrc: './wordpress/wordpress4.jpg',
        category: 'wordpress',
    },
    {
        id: 46,
        imgSrc: './wordpress/wordpress3.jpg',
        category: 'wordpress',
    },
    {
        id: 47,
        imgSrc: './wordpress/wordpress2.jpg',
        category: 'wordpress',
    },
    {
        id: 48,
        imgSrc: './wordpress/wordpress1.jpg',
        category: 'wordpress',
    },
    {
        id: 49,
        imgSrc: './wordpress/wordpress1.jpg',
        category: 'Graphic Design',
    },
    {
        id: 50,
        imgSrc: './wordpress/wordpress1.jpg',
        category: 'landing Pages',
    },
    {
        id: 51,
        imgSrc: './wordpress/wordpress1.jpg',
        category: 'Web Design',
    },
    {
        id: 52,
        imgSrc: './wordpress/wordpress1.jpg',
        category: 'wordpress',
    },


];

const ourWorkBtnCollection = document.querySelectorAll('.our-work-btn button');
const ourWorkImg = document.querySelectorAll('.products');
const productsContainer = document.querySelector('.products-items');
const showMoreBtn = document.querySelector('.products-btn');

let currentCategory = 'all';
let elementsOnPage = 12;
let firstItems = products.slice(0, elementsOnPage);

ourWorkBtnCollection.forEach(btn => {
    btn.addEventListener('click', () => {
        ourWorkBtnCollection.forEach(elem => {
            elem.classList.remove('active-btn');
        })
        btn.classList.add('active-btn');

        const category = btn.getAttribute('data-category');
        const updatedProducts = getProductsByDataCategory(category);

        productsContainer.innerHTML = "";
        displayProducts(updatedProducts.slice(0, 12));
        currentCategory = category;
        elementsOnPage = 12;
        showMoreBtn.style.display = "block";
    })
});

showMoreBtn.addEventListener('click', (btn) => {
    const products = getProductsByDataCategory(currentCategory);
    if (products.length > 12) {
        let newElements = [];

        for (let i = elementsOnPage; i < elementsOnPage + 12; i++) {
            if (products[i]) {
                newElements = [...newElements, products[i]];
            }
        }
        elementsOnPage += newElements.length;
        displayProducts(newElements);
    }

    if (elementsOnPage >= products.length) {
        btn.target.style.display = 'none';
    }
});

displayProducts(firstItems);

function getProductsByDataCategory(category) {
    switch (category) {
        case 'graphic-design': {
            return getProductsByFilterParam('Graphic Design');
        }
        case 'web-design': {
            return getProductsByFilterParam('Web Design');
        }
        case 'landing-page': {
            return getProductsByFilterParam('landing Pages');
        }
        case 'wordpress': {
            return getProductsByFilterParam('wordpress');
        }
        default: {
            return products;
        }
    }
}

function getProductsByFilterParam(category) {
    let productCategoryCollection = [];
    products.forEach(product => {
        if (product.category === category) {
            productCategoryCollection.push(product);
        }
    });
    return productCategoryCollection;
}

function displayProducts(products) {
    products.forEach(product => {
        const flipCard = document.createElement('div');
        flipCard.id = product.id;
        flipCard.className = 'flip-card';

        const flipCardInner = document.createElement('div');
        flipCardInner.className = 'flip-card-inner';
        flipCard.appendChild(flipCardInner);

        const flipCardFront = document.createElement('div');
        flipCardFront.className = 'flip-card-front';
        flipCardInner.appendChild(flipCardFront);

        const img = document.createElement("img");
        img.src = product.imgSrc;
        flipCardFront.appendChild(img);

        const flipCardBack = document.createElement('div');
        flipCardBack.className = 'flip-card-back';
        flipCardInner.appendChild(flipCardBack);


        let xmlns = "http://www.w3.org/2000/svg";
        let boxWidth = 88;
        let boxHeight = 43;

        let svgElem = document.createElementNS(xmlns, "svg");
        svgElem.setAttributeNS(null, "viewBox", "0 0 " + boxWidth + " " + boxHeight);
        svgElem.setAttributeNS(null, "width", boxWidth.toString());
        svgElem.setAttributeNS(null, "height", boxHeight.toString());
        svgElem.setAttributeNS(null, "fill", "none");
        svgElem.setAttributeNS(null, "class", "flip-card-svg");
        svgElem.style.display = "block";
        svgElem.style.margin = "40px auto 15px auto";


        // draw rect
        let elementSvgRect = document.createElementNS(xmlns, "rect");
        elementSvgRect.setAttributeNS(null, "rx", "20")
        elementSvgRect.setAttributeNS(null, "x", "1")
        elementSvgRect.setAttributeNS(null, "y", "2")
        elementSvgRect.setAttributeNS(null, "width", "41")
        elementSvgRect.setAttributeNS(null, "height", "40")
        elementSvgRect.setAttributeNS(null, "stroke", "#18CFAB")


        svgElem.appendChild(elementSvgRect);

        const elementSvgPath = document.createElementNS(xmlns, 'path');
        elementSvgPath.setAttributeNS(null, "fill-rule", "evenodd")
        elementSvgPath.setAttributeNS(null, "clip-rule", "evenodd")
        elementSvgPath.setAttributeNS(null, "d", "M26.9131 17.7282L25.0948 15.8913C24.2902 15.0809 22.983 15.0759 22.1768 15.8826L20.1592 17.8926C19.3516 18.6989 19.3482 20.0103 20.1505 20.8207L21.3035 19.689C21.1868 19.3284 21.3304 18.9153 21.6159 18.6295L22.8995 17.3519C23.3061 16.9462 23.9584 16.9491 24.3595 17.3543L25.4513 18.458C25.8528 18.8628 25.8511 19.5171 25.447 19.9232L24.1634 21.2024C23.8918 21.473 23.4461 21.6217 23.1002 21.5263L21.9709 22.6589C22.7745 23.4718 24.0803 23.4747 24.8889 22.6684L26.9039 20.6592C27.7141 19.8525 27.7167 18.5398 26.9131 17.7282ZM19.5261 25.0918C19.6219 25.4441 19.4686 25.8997 19.1909 26.1777L17.9923 27.3752C17.5807 27.7845 16.916 27.7833 16.5067 27.369L15.393 26.2475C14.9847 25.8349 14.9873 25.1633 15.3982 24.7547L16.598 23.5577C16.8903 23.2661 17.3104 23.1202 17.6771 23.2438L18.8335 22.0715C18.0149 21.2462 16.6825 21.2421 15.8606 22.0632L13.9152 24.0042C13.0923 24.8266 13.0884 26.1629 13.9065 26.9886L15.7582 28.8618C16.576 29.6846 17.9072 29.6912 18.7311 28.8701L20.6765 26.9287C21.4985 26.1054 21.5024 24.7717 20.6855 23.9443L19.5261 25.0918ZM19.2579 24.5631C18.9801 24.8419 18.5343 24.8411 18.2618 24.5581C17.9879 24.2743 17.9901 23.8204 18.2661 23.5399L21.5907 20.1611C21.8668 19.8823 22.3117 19.8831 22.5851 20.164C22.8605 20.4457 22.8588 20.9009 22.5817 21.183L19.2579 24.5631Z");
        elementSvgPath.setAttributeNS(null, "fill", "#1FDAB5");

        svgElem.appendChild(elementSvgPath)

        const elementSvgPath2 = document.createElementNS(xmlns, 'path');
        elementSvgPath2.setAttributeNS(null, "fill-rule", "evenodd")
        elementSvgPath2.setAttributeNS(null, "clip-rule", "evenodd")
        elementSvgPath2.setAttributeNS(null, "d", "M66.5973 1.99795C77.8653 1.99795 86.9999 10.9523 86.9999 21.9979C86.9999 33.0432 77.8653 41.9979 66.5973 41.9979C55.3292 41.9979 46.1946 33.0432 46.1946 21.9979C46.1946 10.9523 55.3292 1.99795 66.5973 1.99795Z");
        elementSvgPath2.setAttributeNS(null, "fill", "#1FDAB5");

        svgElem.appendChild(elementSvgPath2)

        let elementSvgRect2 = document.createElementNS(xmlns, "rect");
        elementSvgRect2.setAttributeNS(null, "x", "60")
        elementSvgRect2.setAttributeNS(null, "y", "17")
        elementSvgRect2.setAttributeNS(null, "width", "12")
        elementSvgRect2.setAttributeNS(null, "height", "11")
        elementSvgRect2.setAttributeNS(null, "fill", "white");

        svgElem.appendChild(elementSvgRect2);

        flipCardBack.appendChild(svgElem)

        const h3 = document.createElement("h3");
        h3.textContent = "creative design";
        h3.className = 'flip-card-title';
        flipCardBack.append(h3);

        const paragraph = document.createElement("p");
        paragraph.textContent = "Web Design";
        paragraph.className = 'flip-card-description';
        flipCardBack.append(paragraph);

        productsContainer.appendChild(flipCard);
    })

}