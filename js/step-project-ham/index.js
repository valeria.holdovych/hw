'use strict'
const ourServicesBtnCollection = document.querySelectorAll('.our-service button');
const ourServicesDescriptionText = document.querySelectorAll('.our-service div');

ourServicesBtnCollection.forEach(btn => {
    btn.addEventListener('click', () => {
        clearElementsClass(ourServicesBtnCollection, 'active-btn');
        addElementClass(btn, 'active-btn');

        const currentAtt = btn.getAttribute('data-category');
        const currentElement = document.getElementById(currentAtt);

        addElementsClass(ourServicesDescriptionText, 'hide-description');
        clearElementsClass(ourServicesDescriptionText, 'active-description');
        clearElementClass(currentElement, 'hide-description');
        addElementClass(currentElement, 'active-description');
    });
})

function clearElementsClass(elements, newClass) {
    elements.forEach(elem => {
        clearElementClass(elem, newClass);
    })
}

function addElementsClass(elements, newClass) {
    elements.forEach(elem => {
        addElementClass(elem, newClass)
    })
}

function addElementClass(element, newClass) {
    element.classList.add(newClass);
}

function clearElementClass(element, newClass) {
    element.classList.remove(newClass);
}



//our work
