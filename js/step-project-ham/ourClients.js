'use strict'

const clients = [
    {
        id: 1,
        name: 'Rihanna',
        position: 'PR manager',
        imgSrc: './clients/badgalriri.jpeg',
        description: 'ё Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa',
    },
    {
        id: 2,
        name: 'Hasan Ali',
        position: 'UX Designer',
        imgSrc: './clients/1561712972_pressa_tv_2.jpeg',
        description: 'Integer dignissim, augue tempus ultricies luctus',
    },
    {
        id: 3,
        name: 'Jim Carrey',
        position: 'Backend Developer',
        imgSrc: './clients/carrey.jpeg',
        description: 'augue tempus ultricies luctus',
    },
    {
        id: 4,
        name: 'Leonardo Dicaprio',
        position: 'Frontend Developer',
        imgSrc: './clients/maxresdefault.jpeg',
        description: 'Integer dignissim, augue tempus ultricies luctus. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus,',
    },
    {
        id: 5,
        name: 'Ann Jameson',
        position: 'UX Designer',
        imgSrc: './clients/portrait61.jpeg',
        description: 'augue tempus ultricies luctus',
    },
    {
        id: 6,
        name: 'Denn Li',
        position: 'Frontend Developer',
        imgSrc: './clients/1.jpeg',
        description: 'Integer dignissim, augue tempus ultricies luctus',
    },
]


const buttonLeft = document.querySelector('.left');
const buttonRight = document.querySelector('.right');
const clientsContainer = document.querySelector('.clients-container');


const mainClient = document.querySelector('.name-of-clients');
// console.log(clientsContainer.childNodes)


initClients();

buttonLeft.addEventListener('click', () => {
    scrollLeft();
})

buttonRight.addEventListener('click', () => {
    scrollRight();
})

function scrollLeft() {
    let currentImages = document.querySelectorAll('.choose-client');
    let activeImage;
    let activeImageIndex;
    let nextImage;

    for (let i = 0; i < currentImages.length; i++) {
        const elem = currentImages[i];
        if (elem.classList.contains('client-active')) {
            activeImage = elem;
            activeImageIndex = i;
        }
    }

    if (activeImage === currentImages[0]) {
        clientsContainer.removeChild(currentImages[3]);

        const firstImage = currentImages[0];
        let nextClient;

        if (firstImage.id === clients[0].id.toString()) {
            nextClient = clients[clients.length - 1];
        } else {
            nextClient = clients[firstImage.id - 2];
        }

        const newImg = createClient(nextClient);
        clientsContainer.insertBefore(newImg, activeImage);
        currentImages = document.querySelectorAll('.choose-client');
        nextImage = currentImages[0];
    } else {
        nextImage = currentImages[activeImageIndex - 1];
    }
    nextImage.classList.add('client-active');
    activeImage.classList.remove('client-active');

    const client = getClientById(nextImage.id);
    changeClient(client);
}


function scrollRight() {
    let currentImages = document.querySelectorAll('.choose-client');
    let activeImage;
    let activeImageIndex;
    let nextImage;

    for (let i = 0; i < currentImages.length; i++) {
        const elem = currentImages[i];
        if (elem.classList.contains('client-active')) {
            activeImage = elem;
            activeImageIndex = i;
        }
    }

    if (activeImage === currentImages[3]) {
        clientsContainer.removeChild(currentImages[0]);

        const lastImage = currentImages[currentImages.length - 1];
        let nextClient;

        if (lastImage.id === clients.length.toString()) {
            nextClient = clients[0];
        } else {
            nextClient = clients[lastImage.id];
        }
        const newImg = createClient(nextClient);
        clientsContainer.appendChild(newImg);
        currentImages = document.querySelectorAll('.choose-client');
        nextImage = currentImages[3];
    } else {
        nextImage = currentImages[activeImageIndex + 1];
    }
    nextImage.classList.add('client-active');
    activeImage.classList.remove('client-active');

    const client = getClientById(nextImage.id);
    changeClient(client);

}

function getClientById(id) {
    let result;
    clients.forEach(client => {
        if (client.id.toString() === id) {
            result = client;
        }
    })
    return result;
}

function initClients() {
    const firstClients = clients.slice(0, 4);

    firstClients.forEach(client => {
        const element = createClient(client);
        if (firstClients[0] === client) {
            element.classList.add('client-active');
            changeClient(client);
        }

        clientsContainer.appendChild(element);
    })
}

function changeClient(client) {
    mainClient.querySelector("h3").innerHTML = client.name;
    mainClient.querySelector("p").innerHTML = client.position;
    mainClient.querySelector("img").src = client.imgSrc;
    document.querySelector('.banner-comments-description').innerHTML = client.description;
}


function createClient(client) {
    const img = document.createElement('img');
    img.id = client.id;
    img.className = 'client-photo choose-client';
    img.src = client.imgSrc;

    img.addEventListener('click', () => {
        changeClient(client);
        const images = document.querySelectorAll('.choose-client');

        images.forEach(e => {
            e.classList.remove('client-active');
        })

        img.classList.add('client-active');
    })

    return img;

}


