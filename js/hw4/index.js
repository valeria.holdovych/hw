'use strict'

let getNum = (message) => {
    let choise;

    do {
        choise = prompt(message, choise)
    }
    while (+isNaN(choise) || choise === '')
    return +choise;
}


let getOperand = (message) => {
    let operand;
    do {
        operand = prompt(message, operand)
    }
    while (operand !== "+" && operand !== "-" && operand !== "*" && operand !== "/")
    return operand;
}


let getResult = () => {
    let firstNum = getNum('Введите первое число'),
        secondNum = getNum('Введите второе число'),
        operandFunc = getOperand('Что вы хотите сделать с числом ? (+, -, *, /)');
    switch (operandFunc) {
        case '+':
            console.log(firstNum + secondNum);
            break;
        case '-':
            console.log(firstNum - secondNum);
            break
        case '/':
            console.log(firstNum / secondNum);
            break;
        case '*':
            console.log(firstNum * secondNum);
            break
    }

}
getResult()

