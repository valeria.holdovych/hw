'use strict'

function filterByFilter(arr, type) {
    let filteredArr = arr.filter(item => typeof (item) !== type)
    return filteredArr;
}


console.log(filterByFilter(['hello', 'world', 23, '23', null], 'string'))



function filterArrayByType (arr, type){
    let result = [];
    arr.forEach((item, index) => {
        if (typeof item !== type) {
            result.push(item)
        }
    })
   return result;
}

console.log(filterArrayByType([5, 10, 23, 3, null], 'number'))