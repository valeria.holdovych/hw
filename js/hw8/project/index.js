'use strict'


const pItem = document.querySelectorAll('p');
pItem.forEach((item) => {
    item.style.background = 'red';
});


const findId = document.querySelector('#optionsList');
console.log(findId);

const findParent = findId.closest('div');
console.log(findParent);

const ndList = findId.childNodes;
ndList.forEach((item) => {
    console.log(item.nodeType)
})
console.log(ndList)


const testParagraphElem = document.getElementById('testParagraph');
const pElem = document.createElement('p');
pElem.innerText = 'This is a paragraph';
testParagraphElem.appendChild(pElem);


const mainHeader = document.querySelector('.main-header');
const mainHeaderElements = mainHeader.querySelectorAll('*');
for (const value of mainHeaderElements) {
    value.classList.add('nav-item');
}
console.log(mainHeaderElements);


const sectionTitleElements = document.querySelectorAll('.section-title');
for (const value of sectionTitleElements) {
    value.classList.remove('section-title');
}
console.log(sectionTitleElements);



