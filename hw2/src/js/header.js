"use strict"


const btn = document.querySelector('.header__nav-button')

btn.addEventListener('click', () => {
    let image = document.querySelector(".header__nav-button img")
    if (document.querySelector(".header__nav").classList.toggle("show")) {
        image.src = './img/menu-button-active.png'
    } else {
        image.src = './img/menu-button.png'
    }
})


// btn.addEventListener('click', () => {
//     let img = document.querySelector('.header__menu-button img')
//     if (document.querySelector(".header__navigation").classList.toggle("show")) {
//         img.src = '../../dist/img/menu-button.png'
//     } else {
//         img.src = '../../dist/img/menu-button-lines.png'
//     }
// })
