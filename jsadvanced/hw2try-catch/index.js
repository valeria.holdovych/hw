'use strict'

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.getElementById('root');
const ul = document.createElement("ul");
div.appendChild(ul);


class BookError extends Error {
    constructor(value) {
        super();
        this.name = "BookError";
        this.message = `Property : ${value} is not defined`;
    }
}


class Book {
    constructor(author, name, price) {
        if(author === undefined){
            throw new BookError('author')
        }
        if(price === undefined){
            throw new BookError('price')
        }
        if(name === undefined){
            throw new BookError('name')
        }
        this.author = author;
        this.name = name;
        this.price = price;

    }

    render(div) {
        div.insertAdjacentHTML("beforeend", `<li>${this.author},${this.name},${this.price}</li>`)
    }
}

books.forEach(e =>{
    try {
        new Book(e.author,e.name, e.price).render(div);
    } catch (err) {
        if (err.name === "BookError") {
            console.warn(err);
        } else {
            throw err;
        }
    }
})

