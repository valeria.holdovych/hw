'use strict'

class Card {
    constructor(username, name, email, title, body, id, userId) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
        this.userId = userId;
        this.container = document.querySelector('.card__wrapper')
        this.card = document.createElement('div');
        this.card.className = 'card'
        this.card.id = `${this.id}`
        this.deleteButton = document.createElement('button')
        this.deleteButton.id = `${this.id}`
    }

    createElements() {
        this.deleteButton.innerHTML = 'Delete'

        this.card.insertAdjacentHTML('beforeend', `
          
           <h3 class="user__nickname">${this.username}</h3>
            <div class="flex">
                <p class="user__name">${this.name}</p>
                <h3 class="user__email">${this.email}</h3>
            </div>
                <h3 class="user__title">${this.title}</h3>
                <p class="user__description">${this.body}</p>
            
        `)

        this.card.append(this.deleteButton);
        this.container.append(this.card);

        this.deleteButton.addEventListener('click', (e) => {

            fetch(`https://ajax.test-danit.com/api/json/users/${this.id}`, {
                method: 'DELETE'
            })
                .then(response => {
                    if(response.status === 200 && this.card.id === this.deleteButton.id){
                        this.card.remove()
                    }
                })
        })
    }

     render(container) {
        this.createElements()
     // document.querySelector(container).append(this.container)
     }
}
Promise.all([
    fetch("https://ajax.test-danit.com/api/json/users").then((res) =>
        res.json().then((data) => data)
    ),
    fetch("https://ajax.test-danit.com/api/json/posts").then((res) =>
        res.json().then((data) => data)
    ),
]).then((data) => {
    const [userArr, postsArr] = data;
    let cardArr = document.querySelector(".card__wrapper");
    postsArr.forEach(({ username, name, email, title, body, id, userId}) => {
        const user = userArr.find((item) => item.id === userId);
        new Card(
            user.username,
            user.name,
            user.email,
            title,
            body,
            id,
            userId,
        ).render(cardArr)})})

    .catch(err => {
        alert("Something went wrong! Please, try to reload the page");
    })
